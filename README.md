# Coding Exercise for OLX Group

This repo demonstrates listing of movies from TMDb. User's can see list of movies and details, and can filter movies on year range. I used MVVM architechture pattern to build my solution.   	

# Featuers
1. List of movies
2. Filter on year range
3. Movie details
4. Paging and Infinite scrolling
5. Unit Tests

# To Run:
1. Run pod install and open CodingExercise.xcworkspace

# Requirements
1. Xcode 11
2. Swift 5
