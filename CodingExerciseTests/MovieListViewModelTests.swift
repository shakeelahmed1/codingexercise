//
//  MovieListViewModelTests.swift
//  CodingExerciseTests
//
//  Created by Shakeel Ahmad on 22/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import XCTest
@testable import CodingExercise

class MovieListViewModelTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSuccessfulMovieLoading() {
        //GIVEN
        let expectation = XCTestExpectation(description: "event wait")
        let viewModel = MovieListViewModel(movieService: MovieServiceMock(mock: MovieAPIResponseMock()))
       
        viewModel.movieListUpdatedBlock = {
           //THEN
           expectation.fulfill()
       }
        //WHEN
        viewModel.load()
        wait(for: [expectation], timeout: 1)
    }
    
    func testMovieLoadingError() {
        //GIVEN
        let expectation = XCTestExpectation(description: "event wait")
        let viewModel = MovieListViewModel(movieService: MovieServiceMockError())
        viewModel.errorBlock = { _ in
            //THEN
            expectation.fulfill()
        }
        //WHEN
        viewModel.load()
        wait(for: [expectation], timeout: 1)
    }
    
    func testPagingParamIsIncrementedAfterLoading() {
        //GIVEN
        let expectation = XCTestExpectation(description: "event wait")
        let mockAPIResponse = MovieAPIResponseMock()
        mockAPIResponse.totalPages = 2
        let viewModel = MovieListViewModel(movieService: MovieServiceMock(mock:mockAPIResponse))
       
        viewModel.movieListUpdatedBlock = {
           //THEN
            if viewModel.currentPage == 2 {
                expectation.fulfill()
            }
       }
        //WHEN
        viewModel.load()
        wait(for: [expectation], timeout: 1)
    }
    
    func testNumberOfRowsPropertyWithoutMovies() {
        let viewModel = MovieListViewModel(movieService: MovieServiceMock(mock: MovieAPIResponseMock()))
        XCTAssert(viewModel.numberOfRows == 1)
    }
    
    func testNumberOfRowsPropertyWithMovies() {
        let mockAPIResponse = MovieAPIResponseMock()
        mockAPIResponse.results = [MovieModelMock()]
        mockAPIResponse.totalPages = 10
        let viewModel = MovieListViewModel(movieService: MovieServiceMock(mock: mockAPIResponse))
        viewModel.load()
        XCTAssert(viewModel.numberOfRows == 2)
    }
    
    func testIsLastCell() {
        let viewModel = MovieListViewModel(movieService: MovieServiceMock(mock: MovieAPIResponseMock()))
        XCTAssert(viewModel.isLastCell(index: 0))
    }
    
    func testIsLastCellAfterLoadingMovies() {
        let mockAPIResponse = MovieAPIResponseMock()
        mockAPIResponse.results = [MovieModelMock()]
        mockAPIResponse.totalPages = 10
        let viewModel = MovieListViewModel(movieService: MovieServiceMock(mock: mockAPIResponse))
        viewModel.load()
        XCTAssert(viewModel.isLastCell(index: 1))
    }
    
    func testAllPagesLoaded() {
        let mockAPIResponse = MovieAPIResponseMock()
        mockAPIResponse.results = [MovieModelMock()]
        mockAPIResponse.totalPages = 1
        let viewModel = MovieListViewModel(movieService: MovieServiceMock(mock: mockAPIResponse))
        viewModel.load()
        XCTAssert(viewModel.pagesLoaded)
    }
}
