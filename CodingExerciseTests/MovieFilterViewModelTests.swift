//
//  MovieFilterViewModelTests.swift
//  CodingExerciseTests
//
//  Created by Shakeel Ahmad on 22/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import XCTest
@testable import CodingExercise

class MovieFilterViewModelTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testValidFilter() {
        let viewModel = MovieFilterViewModel()
        let filter = viewModel.getFilterFrom(startYear: 2018, endYear: 2019)
        XCTAssert(filter != nil && filter?.0 == 2018 && filter?.1 == 2019)
    }
    
    func testInvalidFilter() {
        let viewModel = MovieFilterViewModel()
        XCTAssert(viewModel.getFilterFrom(startYear: 2019, endYear: 2018) == nil)
    }
}
