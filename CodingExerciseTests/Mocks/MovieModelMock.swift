//
//  MovieModelMock.swift
//  CodingExerciseTests
//
//  Created by Shakeel Ahmad on 22/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import Foundation
@testable import CodingExercise

class MovieAPIResponseMock: MovieAPIResponse {
    override init() {
        super.init()
    }
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
}
class MovieModelMock: MovieModel {
    override init() {
        super.init()
    }
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
}
