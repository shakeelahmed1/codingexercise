//
//  MovieServiceMock.swift
//  CodingExerciseTests
//
//  Created by Shakeel Ahmad on 22/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import Foundation
@testable import CodingExercise

class MovieServiceMock: MovieServiceProtocol {
    let mockObj: MovieAPIResponseMock
    init(mock: MovieAPIResponseMock) {
        mockObj = mock
    }
    func getMoviesList(page: String, minYear: Int?, maxYear: Int?, completion: @escaping (APIResult<MovieAPIResponse>) -> Void) {
        completion(APIResult.success(mockObj))
    }
}

class MovieServiceMockError: MovieServiceProtocol {
    init() {}
    func getMoviesList(page: String, minYear: Int?, maxYear: Int?, completion: @escaping (APIResult<MovieAPIResponse>) -> Void) {
        completion(APIResult.error("Mock error"))
    }
}
