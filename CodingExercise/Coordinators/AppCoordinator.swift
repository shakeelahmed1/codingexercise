//
//  AppCoordinator.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import UIKit

class AppCoordinator {
    private let listingStoryboardName = "MovieListing"
    
    func configureRootController(window: UIWindow?) {
        let navController: UINavigationController = UIStoryboard(name: listingStoryboardName, bundle: Bundle.main).initialViewController()
        if let movieListingController = navController.viewControllers.first as? MovieListViewController {
            movieListingController.viewModel = MovieListViewModel(movieService: MovieService(networkManager: NetworkManager()))
        }
        window?.rootViewController = navController
        window?.makeKeyAndVisible()
    }
}
