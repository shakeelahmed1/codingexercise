//
//  MovieDetailsCoordinator.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import UIKit

class MovieDetailsCoordinator {
    private let detailsStoryboardName = "MovieDetails"
    
    func showDetailsController(controller: UIViewController, movie: MovieModel) {
        let movieDetailsController: MovieDetailsViewController = UIStoryboard(name: detailsStoryboardName, bundle: Bundle.main).initialViewController()
         movieDetailsController.viewModel = MovieDetailViewModel(movie: movie)
        controller.navigationController?.pushViewController(movieDetailsController, animated: true)
    }
}
