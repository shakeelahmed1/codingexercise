//
//  MovieModel.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import Foundation

open class MovieAPIResponse: Codable {
    var page: Int
    var totalResults: Int
    var totalPages: Int
    var results: [MovieModel]
    
    enum CodingKeys: String, CodingKey {
        case page, results
        case totalResults = "total_results"
        case totalPages = "total_pages"
    }
    
    public init() {
        page = 0
        totalPages = 0
        totalResults = 0
        results = []
    }
}
open class MovieModel: Codable {
    var posterPath: String?
    var adult: Bool
    var popularity: Double
    var backdropPath: String?
    var originalLanguage, originalTitle: String
    var title: String
    var overview, releaseDate: String

    enum CodingKeys: String, CodingKey {
        case posterPath = "poster_path"
        case adult, popularity
        case backdropPath = "backdrop_path"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case title
        case overview
        case releaseDate = "release_date"
    }
    
    public init() {
        adult = false
        popularity = 0
        originalTitle = ""
        originalLanguage = ""
        title = ""
        overview = ""
        releaseDate = ""
    }
}
