//
//  MovieService.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import Foundation

protocol MovieServiceProtocol {
    func getMoviesList(page: String, minYear: Int?, maxYear: Int?, completion: @escaping (APIResult<MovieAPIResponse>)-> Void)
}

class MovieService: MovieServiceProtocol {
    private let networkManager: NetworkManagerProtocol
    
    init(networkManager: NetworkManagerProtocol) {
        self.networkManager = networkManager
    }
    
    func getMoviesList(page: String, minYear: Int?, maxYear: Int?, completion: @escaping (APIResult<MovieAPIResponse>)-> Void) {
        let router: MovieRouters = .movieList(page: page, minYear: minYear, maxYear: maxYear)
        networkManager.requestObject(router) { (data: Data?, error: String?) in
             guard let data = data, let movieApiModel = try? JSONDecoder().decode(MovieAPIResponse.self, from: data) else {
                completion(APIResult.error(error ?? "JSON parsing issue occurred."))
                return
            }
            completion(APIResult.success(movieApiModel))
        }
    }
}
