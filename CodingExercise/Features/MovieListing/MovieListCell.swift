//
//  MovieListCell.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import UIKit
import Nuke

class MovieListCell: UITableViewCell {
    
    //MARK:- IBOutlets
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    //MARK:- Initialize Cell UI
    func initGUI(model: Model) {
        titleLabel.text = model.title
        overviewLabel.text = model.overview
        dateLabel.text = model.date
        if let url = model.imageURL {
            Nuke.loadImage(with: url, into: movieImageView)
        } else {
            movieImageView.image = UIImage(named: "placeholder")
        }
    }
}
extension MovieListCell {
    struct Model {
        let title: String
        let overview: String
        let date: String
        let imageURL: URL?
    }
}
