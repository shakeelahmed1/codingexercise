//
//  MovieListViewModel.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import Foundation

class MovieListViewModel {
    
    private var page: Int
    private var movies: [MovieModel]
    private let movieService: MovieServiceProtocol
    private var isLoading: Bool
    private var allPagesLoaded: Bool
    
    /// Filters movies list on year range
    var filter: (minY: Int, maxY: Int)? {
        didSet {
            page = 1
            movies = []
            allPagesLoaded = false
            movieListUpdatedBlock?()
        }
    }
    
    /// Reload table view hook for ViewController
    var movieListUpdatedBlock: (()->Void)? = nil
    
    /// Error hook for ViewController
    var errorBlock: ((String)->Void)? = nil
    
    init(movieService: MovieServiceProtocol) {
        self.movieService = movieService
        page = 1
        filter = nil
        isLoading = false
        allPagesLoaded = false
        movies = []
    }
    
    private func getThumbURL(posterPath: String?) -> URL? {
        guard let path = posterPath else { return nil }
        return URL(string: "\(Constants.BASE_IMAGE_URL)\(path)")
    }
    
    /// Loads moveis list
    func load() {
        guard !isLoading else {return}
        isLoading = true
        movieService.getMoviesList(page: "\(page)", minYear: filter?.minY, maxYear: filter?.maxY) { [weak self] (result) in
            switch result {
            case .success(let movieAPIModel):
                if (self?.page ?? 0) == movieAPIModel.totalPages {
                    self?.allPagesLoaded = true
                }
                self?.movies += movieAPIModel.results
                self?.page += 1
                self?.movieListUpdatedBlock?()
            case .error(let message):
                self?.errorBlock?(message)
            }
            DispatchQueue.global(qos: .background).async {
                self?.isLoading = false
            }
        }
    }
    
    subscript(index: Int) -> MovieModel {
        get {
            return movies[index]
        }
    }
    
    var currentPage: Int {return page}
    var pagesLoaded: Bool {return allPagesLoaded}
    
    //MARK:- UITableview data related methods
    var numberOfRows: Int {
        return allPagesLoaded ? movies.count : movies.count + 1
    }
    func isLastCell(index: Int) -> Bool {
        return !allPagesLoaded && index == movies.count
    }
    func cellModelForRow(index: Int) -> MovieListCell.Model {
        let movie = movies[index]
        return MovieListCell.Model(title: movie.title, overview: movie.overview, date: "Released on \(movie.releaseDate.dateFormatted)",
                                   imageURL: getThumbURL(posterPath: movie.posterPath))
    }
}
