//
//  MovieListViewController.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import UIKit

protocol MovieFilterProtocol: NSObjectProtocol {
    func filterMovies(with filter: (Int, Int))
}

class MovieListViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Data Members
    private let movieCellIdentifier = "MovieListCell"
    private let loadingCellIdentifier = "LoadingCell"
    let FILTER_SEGUE_IDENTIFIER = "FilterSegue"
    
    //MARK:- Injected Properties
    var viewModel: MovieListViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        bindViewModel()
    }
    
    private func bindViewModel() {
        viewModel.movieListUpdatedBlock = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        viewModel.errorBlock = { [weak self] message in
            DispatchQueue.main.async {
                self?.showErrorAlert(message: message)
            }
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == FILTER_SEGUE_IDENTIFIER {
            let destination = segue.destination as! MovieFilterViewController
            destination.delegate = self
        }
    }
}

extension MovieListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if viewModel.isLastCell(index: indexPath.row) {
            viewModel.load()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard !viewModel.isLastCell(index: indexPath.row) else {
            return tableView.dequeueReusableCell(withIdentifier: loadingCellIdentifier, for: indexPath)
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: movieCellIdentifier, for: indexPath) as! MovieListCell
        cell.initGUI(model: viewModel.cellModelForRow(index: indexPath.row))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        MovieDetailsCoordinator().showDetailsController(controller: self, movie: viewModel[indexPath.row])
    }
}

extension MovieListViewController: MovieFilterProtocol {
    func filterMovies(with filter: (Int, Int)) {
        viewModel.filter = filter
    }
}
