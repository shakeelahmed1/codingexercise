//
//  MovieFilterViewController.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import UIKit

class MovieFilterViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var startYearPicker: UIPickerView!
    @IBOutlet weak var endYearPicker: UIPickerView!
    
    //MARK:- Data members
    private let viewModel = MovieFilterViewModel()
    
    //MARK:- Injected Properties
    weak var delegate: MovieFilterProtocol?
    
    //MARK:- IBActions
    @IBAction func actionClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func actionDone(_ sender: Any) {
        let startYear = viewModel[startYearPicker.selectedRow(inComponent: 0)]
        let endYear = viewModel[endYearPicker.selectedRow(inComponent: 0)]
        guard let filter = viewModel.getFilterFrom(startYear: startYear, endYear: endYear) else {
            showErrorAlert(message: "Wrong selection. Start year should be less than or equal to end year.")
            return
        }
        delegate?.filterMovies(with: filter)
        dismiss(animated: true, completion: nil)
    }
}

extension MovieFilterViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.totalYearsCount
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel.titleForIndex(index: row)
    }
}
