//
//  MovieFilterViewModel.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import Foundation

class MovieFilterViewModel {
    
    private let years: [Int]
    
    init() {
        let currentYear = Calendar.current.component(.year, from: Date())
        years = Array(1900...currentYear).reversed()
    }
    
    subscript(index: Int) -> Int {
        get { return years[index] }
    }
    var totalYearsCount: Int {
        return years.count
    }
    func titleForIndex(index: Int) -> String {
        return "\(years[index])"
    }
    func getFilterFrom(startYear: Int, endYear: Int) -> (Int, Int)? {
        guard startYear <= endYear else {return nil}
        return (startYear, endYear)
    }
}
