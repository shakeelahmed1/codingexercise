//
//  MovieDetailViewModel.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import Foundation

class MovieDetailViewModel {
    
    private let movie: MovieModel
    private var keyValues: [(String, String)]
    
    init(movie: MovieModel) {
        self.movie = movie
        keyValues = [
            ("Title", movie.title),
            ("Original Title", movie.originalTitle),
            ("Release Date", "Released on \(movie.releaseDate.dateFormatted)"),
            ("Overview", movie.overview),
            ("Language", movie.originalLanguage),
            ("Popularity", "\(movie.popularity)"),
            ("Adult", "\(movie.adult)")
        ]
    }
    
    func getImageURL() -> URL? {
        let path = movie.backdropPath ?? (movie.posterPath ?? "")
        return URL(string: "http://image.tmdb.org/t/p/w400/\(path)")
    }
    
    //MARK:- UITableview data related methods
    var numberOfRows: Int {
        return keyValues.count
    }
   
    func cellModelForRow(index: Int) -> (String, String) {
        return keyValues[index]
    }
}
