//
//  MovieDetailsViewController.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import UIKit
import Nuke

class MovieDetailsViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var detailTableview: UITableView!
    
    //MARK:- Data Members
    private let detailCellIdentifier = "DetailCell"
      
    //MARK:- Injected Properties
    var viewModel: MovieDetailViewModel!
    
    //MARK:- View Life Cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initGUI()
    }
    
    //MARK:- View Initialization
    private func initGUI() {
        if let url = viewModel.getImageURL() {
            Nuke.loadImage(with: url, into: movieImageView)
        } else {
            movieImageView.image = UIImage(named: "placeholder")
        }
    }
}

extension MovieDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: detailCellIdentifier, for: indexPath) as! MovieDetailsCell
        let (title, description) = viewModel.cellModelForRow(index: indexPath.row)
        cell.initGUI(title: title, description: description)
        return cell
    }
}
