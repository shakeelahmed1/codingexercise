//
//  MovieDetailsCell.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import UIKit

class MovieDetailsCell: UITableViewCell {

    //MARK:- IBOutlets
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var keyLabel: UILabel!

    //MARK:- View Initialization
    func initGUI(title: String, description: String) {
        keyLabel.text = title
        valueLabel.text = description
    }
}
