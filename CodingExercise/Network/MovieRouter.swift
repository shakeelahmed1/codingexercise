//
//  MovieRouter.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import Foundation

enum MovieRouters: RouterConfiguration {
    case movieList(page: String, minYear: Int?, maxYear: Int?)
    
    var apiURL: URL? {
        switch self {
        case .movieList(let page, let minYear, let maxYear):
            var urlComp = URLComponents(string: "\(baseURL)/discover/movie")
            var queryItems = [URLQueryItem(name: Constants.LANGUAGE_KEY, value: Constants.EN_US_KEY),
                              URLQueryItem(name: Constants.SORT_BY_KEY, value: Constants.POPULARITY_DESC_KEY),
                              URLQueryItem(name: Constants.PAGE_KEY, value: page),
                              URLQueryItem(name: Constants.API_KEY_PARAM_NAME, value: Constants.API_KEY)]
            if let minY = minYear {
                queryItems.append(URLQueryItem(name: Constants.MIN_RELEASE_DATE_KEY, value: "\(minY)-01-01"))
            }
            if let maxY = maxYear {
                queryItems.append(URLQueryItem(name: Constants.MAX_RELEASE_DATE_KEY, value: "\(maxY)-01-01"))
            }
            urlComp?.queryItems = queryItems
            guard let url = urlComp?.url else {return nil}
            return url
        }
    }
}
