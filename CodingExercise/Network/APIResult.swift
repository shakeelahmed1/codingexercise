//
//  APIResult.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import Foundation

enum APIResult<T> {
    case success(T)
    case error(String)
}
