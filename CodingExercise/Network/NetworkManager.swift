//
//  NetworkManager.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import Foundation

protocol NetworkManagerProtocol {
    func requestObject(_ router: RouterConfiguration, completion: @escaping ((Data?, String?) -> Void))
}
class NetworkManager: NetworkManagerProtocol {
    func requestObject(_ router: RouterConfiguration, completion: @escaping ((Data?, String?) -> Void)) {
        let session = URLSession(configuration: URLSessionConfiguration.default)
        guard let request = router.asURLRequest() else {
            completion(nil, "Invalid URL generated.")
            return
        }
        session.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                completion(nil, error?.localizedDescription)
                return
            }
            completion(data, nil)
        }.resume()
    }
}
