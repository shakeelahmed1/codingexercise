//
//  RouterConfiguration.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import Foundation

protocol RouterConfiguration {
    var method: String { get }
    var apiURL: URL? { get }
    var baseURL: String { get }
}

extension RouterConfiguration {
    var baseURL: String {
        return Constants.BASE_API_URL
    }
    var method: String { return "get" }
}

extension RouterConfiguration {
    public func asURLRequest() -> URLRequest? {
        guard let apiURL = apiURL else { return nil }
        var urlRequest = URLRequest(url: apiURL)
        urlRequest.httpMethod = method
        return urlRequest
    }
}
