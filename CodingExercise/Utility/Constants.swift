//
//  Constants.swift
//  CodingExercise
//
//  Created by Shakeel Ahmad on 21/12/2019.
//  Copyright © 2019 OLX Group Dubai. All rights reserved.
//

import Foundation

struct Constants {
    static let BASE_API_URL = "https://api.themoviedb.org/3"
    static let BASE_IMAGE_URL = "http://image.tmdb.org/t/p/w200/"
    static let MIN_RELEASE_DATE_KEY = "primary_release_date.gte"
    static let MAX_RELEASE_DATE_KEY = "primary_release_date.lte"
    static let LANGUAGE_KEY = "language"
    static let EN_US_KEY = "en-US"
    static let SORT_BY_KEY = "sort_by"
    static let POPULARITY_DESC_KEY = "popularity.desc"
    static let API_KEY = "8af3974d84c00db421f822c6e2bdb6ff"
    static let API_KEY_PARAM_NAME = "api_key"
    static let PAGE_KEY = "page"
}
